all: html/id.txt html/favicon.ico

html/id.txt: _id.txt
	gpg2 --clearsign -o $@ $<

html/favicon.ico: TMPDIR:=$(shell mktemp -d --tmpdir favicon.XXXXXXXXXX)
html/favicon.ico: html/img/emblem.svg
	convert -resize 16x16 -background none $^ ${TMPDIR}/favicon-16.png
	convert -resize 32x32 -background none $^ ${TMPDIR}/favicon-32.png
	convert ${TMPDIR}/favicon-16.png ${TMPDIR}/favicon-32.png $@
	rm -rf ${TMPDIR}
