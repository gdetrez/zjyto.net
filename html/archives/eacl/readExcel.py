# -*- coding: utf-8 -*-
import csv
import json
from datetime import datetime, timedelta

# Helper functions

null = None

def i(matrix,char,num):
	alpha = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI'.split()
	letter = alpha.index(char)
	return matrix[num-1][letter] #corresponds to the indexing that Excel shows

def info(matrix,letter,startnum): 
    for num in range(startnum,100): #grow number if more stuff
        maybeValue = i(matrix, letter, num)
        if maybeValue != "":
            return (maybeValue,num) #return num where it ended so we know where to continue
        else: pass
    else: return None

def endsection(matrix,start):
    #gives the row where the next title starts
    title1,end = info(matrix,'B',start)
    title2,end_ = info(matrix,'B',end)
    if end_ - end <= 1:
        title2,end2 = info(matrix,'B',start+1)
        return end2
    else:
        return end_

def date(matrix,startnum):
    #Monday, April 28
    rawdate,e1 = info(matrix, 'A', startnum)
    date = "2014-04-%s" % rawdate.split()[2] #28
    return date

def time(matrix,startnum):
    #e.g. 15:00-16:00
    rawtime,e2 = info(matrix,'E',startnum)
    rawtimes = rawtime.split('-') 
    
    #e.g. 15:00
    start = rawtimes[0]
    
    time1 = datetime.strptime(rawtimes[0], "%H:%M")
    time2 = datetime.strptime(rawtimes[1], "%H:%M")
    #e.g. 1:00:00
    timediff = str(time2 - time1)
    
    #time-related stuff in same tuple
    return (start, timediff)
    
def papers(matrix,startnum):
    
    # just a single paper
    def paper(matrix,startnum):
        if i(matrix,'T',startnum) == "":
            return None
        else:
            pid      = i(matrix,'T',startnum)
            ptitle   = i(matrix,'Z',startnum)
            pauthors = i(matrix,'AA',startnum)
            pcorresp = i(matrix,'AC',startnum)
            pemail   = i(matrix,'AE',startnum)
            return {"id":pid,
                 "title":ptitle,
               "authors":pauthors.split('and'),
               "corresp":pcorresp,
                "email" :pemail}


    endnum=endsection(matrix,startnum)
    
    #all papers in section    
    allpapers = []
    for num in range(startnum,endnum):
        ppr = paper(matrix,num)
        if ppr != None: allpapers.append(ppr)
        else: pass
        
    return (allpapers,endnum)

# Putting together one section
def section(matrix,startnum,date_):
    start,timediff = time(matrix,startnum)
    title,endnum=info(matrix,'B',startnum)
    secID,endnum=info(matrix,'D',startnum)
    chair,endnum=info(matrix,'H',startnum)
    return ({"id": secID,
    "logo":null,
    "date":date_,
    "start":start,
    "duration":timediff,
    "room":"1",
    "slug":"",
    "title":title,
    "subtitle":"",
    "track":null,
    "type":"lecture",
    "papers":papers(matrix,startnum)[0],
    "language":"",
    "abstract":"",
    "description":"",
    "persons":[chair],
    "links":[]},
    endnum)
    
# Collecting all sections into a list
def sections(matrix,startnum,date_):
    def sechelp(matrix,startnum,date_):
        if (startnum>=53): #change number according to spreadsheet
            return []
        else:    
            s,end=section(matrix,startnum,date_)
            return [s] + sechelp(matrix,end+1,date_)
            
    date_ = date(matrix,startnum)
    return sechelp(matrix,startnum,date_)
     
def day(matrix,startnum,idx):
    date_ = date(matrix,startnum)
    daystart = date_ + "T09:00:00+02:00"
    dayend   = date_ + "T17:30:00+02:00"
    secs = sections(matrix,startnum,date_)
    rooms = {"1":secs}
    return {"index":idx,
            "date":date_,
            "day_start":daystart,
            "day_end":dayend,
            "rooms":rooms}
        
        
if __name__ == "__main__":
    with open('eaclCSV.csv','rb') as file:
        contents = csv.reader(file, delimiter=';')
        matrix = list()
        for row in contents:
            matrix.append(row)


    # foo=open("foo","w")
    # foo.write( sections(matrix,2) )
    day_ = day(matrix,2,3)
    print day_
