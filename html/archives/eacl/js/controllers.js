'use strict';

/* Controllers */

angular.module('myApp.controllers', ['myApp', 'ngSanitize']).
  controller('MyCtrl1', ['$scope', '$http', 'ScheduleURL', function($scope, $http, url) {
    $http.get(url).success(function(data) {
      console.log(data.schedule)
      $scope.schedule = data.schedule;
      $scope.days = [];
      angular.forEach(data.schedule.conference.days, function(value, key){
        var events = _.flatten(_.values( value.rooms ))
      console.log(events)

        this.push({ schedule: _.groupBy( events, 'start' ) });
      }, $scope.days);
    });

  }])
  .controller('MyCtrl2', [function() {

  }]);
