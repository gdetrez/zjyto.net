// Setup nice drag animation on the emblem
$(function() {
    var element = document.querySelector("#emblem");
    // Disable default drag action on firefox:
    $(element).on("dragstart", function() { return false; });
    var hammertime = new Hammer(element);
    var ticking = false;
    function translateElement( x, y, t) {
        if (t)
            element.style.transition = "all " + t + "s";
        else
            element.style.transition = "";
        element.style.transform = 'translate3d(' + x + 'px,' + y + 'px,0)';
    }

    hammertime.on("hammer.input", function(ev) {
        if(ev.isFinal)
            translateElement( 0, 0, .3);
        //console.log(ev);
    });
    hammertime.on('panstart panmove', function(ev) {
        translateElement(ev.deltaX, ev.deltaY);
    });
});
